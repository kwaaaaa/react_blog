Rails.application.routes.draw do
  get 'home/about', as: 'about', path:'about'

  get 'home/services', as: 'services', path:'services'

  get 'home/contact', as: 'contact', path:'contact'

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  devise_for :users
  resources :posts, only: [:index, :show, :create, :destroy] do
    resources :comments, only: [:index, :create, :destroy]
  end
  resources :categories
  root 'posts#index'
end

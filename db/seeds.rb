if User.count == 0
  User.create!(email: 'user@example.com', password: 'Password', password_confirmation: 'Password', role: 'user')
  User.create!(email: 'guest@example.com', password: 'Password', password_confirmation: 'Password', role: 'guest')
  User.create!(email: 'another_user@example.com', password: 'Password', password_confirmation: 'Password', role: 'user')
  User.create!(email: 'another_guest@example.com', password: 'Password', password_confirmation: 'Password', role: 'guest')
end


if Category.count == 0
  Category.create!(name: 'Ruby')
  Category.create!(name: 'Ruby on Rails')
  Category.create!(name: 'AngularJS')
  Category.create!(name: 'Other')
  Category.create!(name: 'Eazy come, eazy go')
end


if Post.count == 0
  User.find_by(email: 'user@example.com').posts.create!(title: 'Active Record Associations',
               body: 'In Rails, an association is a connection between two Active Record models.
               Why do we need associations between models? Because they make common operations simpler and easier in your code.
               For example, consider a simple Rails application that includes a model for authors and a model for books.
               Each author can have many books. Without associations, the model declarations would look like this:',
               category_ids: Category.find_by(name: 'Ruby on Rails').id, image: File.new("#{Rails.root}/app/assets/images/rails.png"))
  User.find_by(email: 'user@example.com').posts.create!(title: 'RubyMine 8 product key',
               body: 'https://xn--90agrrk8e.xn--p1ai/rubymine-2016-product-key/',
               category_ids: Category.find_by(name: 'Eazy come, eazy go').id, image: File.new("#{Rails.root}/app/assets/images/rails.png"))
  User.find_by(email: 'user@example.com').posts.create!(title: 'Raesent accumsan',
               body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sollicitudin sem mauris,
               egestas euismod lacus luctus ac. Duis et nisi non eros pellentesque sodales id ac lacus.
               Pellentesque nulla sapien, aliquam sed orci eu, rhoncus faucibus diam. Etiam non maximus enim.
               Quisque eleifend sodales placerat. Quisque bibendum blandit eros sit amet interdum.
               Praesent accumsan risus interdum mattis facilisis.',
               category_ids: Category.find_by(name: 'Other').id, image: File.new("#{Rails.root}/app/assets/images/rails.png"))
  User.find_by(email: 'another_user@example.com').posts.create!(title: 'Consectetur sodales',
               body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sollicitudin sem mauris,
               egestas euismod lacus luctus ac. Duis et nisi non eros pellentesque sodales id ac lacus.
               Pellentesque nulla sapien, aliquam sed orci eu, rhoncus faucibus diam. Etiam non maximus enim.
               Quisque eleifend sodales placerat. Quisque bibendum blandit eros sit amet interdum.
               Praesent accumsan risus interdum mattis facilisis.',
               category_ids: Category.find_by(name: 'Other').id, image: File.new("#{Rails.root}/app/assets/images/rails.png"))
  User.find_by(email: 'another_user@example.com').posts.create!(title: 'Quisque bibendum',
               body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sollicitudin sem mauris,
               egestas euismod lacus luctus ac. Duis et nisi non eros pellentesque sodales id ac lacus.
               Pellentesque nulla sapien, aliquam sed orci eu, rhoncus faucibus diam. Etiam non maximus enim.
               Quisque eleifend sodales placerat. Quisque bibendum blandit eros sit amet interdum.
               Praesent accumsan risus interdum mattis facilisis.',
               category_ids: Category.find_by(name: 'Other').id, image: File.new("#{Rails.root}/app/assets/images/rails.png"))
end


if Comment.count == 0
  Post.find_by(title: 'Active Record Associations').comments.create!(title: 'Solor lacus',
               body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sollicitudin sem mauris,
               egestas euismod lacus luctus ac. Duis et nisi non eros pellentesque sodales id ac lacus.
               Pellentesque nulla sapien, aliquam sed orci eu, rhoncus faucibus diam. Etiam non maximus enim.
               Quisque eleifend sodales placerat. Quisque bibendum blandit eros sit amet interdum.
               Praesent accumsan risus interdum mattis facilisis.')
  Post.find_by(title: 'Active Record Associations').comments.create!(title: 'Solor lacus',
               body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sollicitudin sem mauris,
               egestas euismod lacus luctus ac. Duis et nisi non eros pellentesque sodales id ac lacus.
               Pellentesque nulla sapien, aliquam sed orci eu, rhoncus faucibus diam. Etiam non maximus enim.
               Quisque eleifend sodales placerat. Quisque bibendum blandit eros sit amet interdum.
               Praesent accumsan risus interdum mattis facilisis.')
  Post.find_by(title: 'Active Record Associations').comments.create!(title: 'Solor lacus',
               body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sollicitudin sem mauris,
               egestas euismod lacus luctus ac. Duis et nisi non eros pellentesque sodales id ac lacus.
               Pellentesque nulla sapien, aliquam sed orci eu, rhoncus faucibus diam. Etiam non maximus enim.
               Quisque eleifend sodales placerat. Quisque bibendum blandit eros sit amet interdum.
               Praesent accumsan risus interdum mattis facilisis.')
  Post.find_by(title: 'Active Record Associations').comments.create!(title: 'Solor lacus',
               body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sollicitudin sem mauris,
               egestas euismod lacus luctus ac. Duis et nisi non eros pellentesque sodales id ac lacus.
               Pellentesque nulla sapien, aliquam sed orci eu, rhoncus faucibus diam. Etiam non maximus enim.
               Quisque eleifend sodales placerat. Quisque bibendum blandit eros sit amet interdum.
               Praesent accumsan risus interdum mattis facilisis.')



  Post.find_by(title: 'Raesent accumsan').comments.create!(title: 'Raesent accumsan',
               body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sollicitudin sem mauris,
               egestas euismod lacus luctus ac. Duis et nisi non eros pellentesque sodales id ac lacus.
               Pellentesque nulla sapien, aliquam sed orci eu, rhoncus faucibus diam. Etiam non maximus enim.
               Quisque eleifend sodales placerat. Quisque bibendum blandit eros sit amet interdum.
               Praesent accumsan risus interdum mattis facilisis.')
  Post.find_by(title: 'Raesent accumsan').comments.create!(title: 'Raesent accumsan',
               body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sollicitudin sem mauris,
               egestas euismod lacus luctus ac. Duis et nisi non eros pellentesque sodales id ac lacus.
               Pellentesque nulla sapien, aliquam sed orci eu, rhoncus faucibus diam. Etiam non maximus enim.
               Quisque eleifend sodales placerat. Quisque bibendum blandit eros sit amet interdum.
               Praesent accumsan risus interdum mattis facilisis.')
  Post.find_by(title: 'Raesent accumsan').comments.create!(title: 'Raesent accumsan',
               body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sollicitudin sem mauris,
               egestas euismod lacus luctus ac. Duis et nisi non eros pellentesque sodales id ac lacus.
               Pellentesque nulla sapien, aliquam sed orci eu, rhoncus faucibus diam. Etiam non maximus enim.
               Quisque eleifend sodales placerat. Quisque bibendum blandit eros sit amet interdum.
               Praesent accumsan risus interdum mattis facilisis.')
  Post.find_by(title: 'Raesent accumsan').comments.create!(title: 'Raesent accumsan',
               body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sollicitudin sem mauris,
               egestas euismod lacus luctus ac. Duis et nisi non eros pellentesque sodales id ac lacus.
               Pellentesque nulla sapien, aliquam sed orci eu, rhoncus faucibus diam. Etiam non maximus enim.
               Quisque eleifend sodales placerat. Quisque bibendum blandit eros sit amet interdum.
               Praesent accumsan risus interdum mattis facilisis.')

end

if AdminUser.count == 0
  AdminUser.create!(email: 'admin@example.com', password: 'Password', password_confirmation: 'Password')
end
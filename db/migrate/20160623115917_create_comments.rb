class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.string :title, default: '', null: false, default: '', null: false
      t.text :body, default: '', null: false
      t.references :user, index: true, foreign_key: true
      t.references :post, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end

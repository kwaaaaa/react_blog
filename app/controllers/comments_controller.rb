class CommentsController < ApplicationController
  before_action :authenticate_user!
  load_resource
  authorize_resource only: [:create, :destroy]
  before_action :set_comment, only: [:show, :edit, :update, :destroy]
  respond_to :json

  def index
    @comments = Comment.where(post_id: params[:post_id])
    # @comments = Comment.find_by(post_id: params[:post_id])
    respond_with(@comments) do |format|
      format.json { render json: @comments }
      format.html
    end
  end

  def create
    @comment = current_user.comments.new(comment_params)
    post = Post.find(params[:post_id])
    respond_to do |format|
      if @comment.save
        format.html { redirect_to post, notice: 'Comment was successfully created.' }
        format.json { render :show, status: :created, location: post }
      else
        format.html { render :new }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @comment.destroy
    respond_to do |format|
      format.html { redirect_to comments_url, notice: 'Comment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_comment
      @comment = Comment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def comment_params
      params.require(:comment).permit(:title, :body, :post_id)
    end
end


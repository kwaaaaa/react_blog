class PostsController < ApplicationController
  before_action :authenticate_user!, only: [:create, :destroy]
  load_resource
  authorize_resource only: [:create, :destroy]
  before_action :set_post, only: [:show, :destroy]
  respond_to :json

  def index
    @post = Post.new
    @posts = Post.all.order('created_at DESC')
    respond_with(@posts) do |format|
      format.json { render json: @posts.to_json(include: [ :categories, :user ]) }
      format.html
    end
  end

  def show
    respond_to do |format|
      format.html
      format.json { render json: @post.to_json(:include => :categories) }
    end
  end

  def create
    @post = current_user.posts.new(post_params)
    # @post.add_categories(params)

    respond_to do |format|
      if @post.save
        format.html { redirect_to root_path, notice: 'Post was successfully created.' }
        format.json { render json: @post.to_json(:include => :categories) }
      else
        format.html { render :index }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

    def destroy
      @post.destroy
      respond_to do |format|
        format.html { redirect_to posts_url, notice: 'Post was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private

    def set_post
      @post = Post.find(params[:id])
    end

    def post_params
      # binding.pry
      params.require(:post).permit(:title, :body, {category_ids: []}, :image)
    end

end
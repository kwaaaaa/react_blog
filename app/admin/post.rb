ActiveAdmin.register Post do
  actions :index
  index do
    selectable_column
    id_column
    column :title
    column :category
    column :body
    column :created_at
  end

  filter :title
  filter :body
  filter :category
  filter :sign_in_count
  filter :created_at


end

ActiveAdmin.register User do
  permit_params :email, :password, :password_confirmation
  actions :index
  index do
    selectable_column
    id_column
    column :email
    column :sign_in_count
    column :created_at
  end

  filter :email
  filter :current_sign_in_at
  filter :sign_in_count
  filter :created_at
end

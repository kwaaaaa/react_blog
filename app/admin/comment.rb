ActiveAdmin.register Comment, :as => "User Comments" do
  actions :index
  index do
    selectable_column
    id_column
    column :title
    column :body
    column :created_at
  end

  filter :title
  filter :body
  filter :sign_in_count
  filter :created_at

end

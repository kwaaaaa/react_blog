@PostForm = React.createClass
  getInitialState: ->
    title: ''
    body: ''
    image: ''
    categories_ids: []
  render: ->
    React.DOM.div
      className: 'well'
      React.DOM.h4
        className: 'form-title'
        'Leave a Post:'
      React.DOM.form
        onSubmit: @handleSubmit
        React.DOM.div
          className: 'form-group'
          React.DOM.input
            type: 'text'
            className: 'form-control'
            placeholder: 'Title'
            name: 'title'
            rows: 3
            value: @state.title
            onChange: @handleChange
          React.DOM.textarea
            className: 'form-control'
            placeholder: 'Body'
            name: 'body'
            rows: 3
            value: @state.body
            onChange: @handleChange
          React.DOM.input
            type: 'file'
            name: 'image'
            onChange: @handleChangeFile
        React.DOM.button
          type: 'submit'
          className: 'btn btn-primary'
          'Submit'
  handleSubmit: (e) ->
    e.preventDefault()
    $.post '/posts/', { post: @state }, (data) =>
      @props.handleNewPost data
      @setState @getInitialState()
    , 'JSON'
  handleChange: (e) ->
    name = e.target.name
    @setState "#{ name }": e.target.value
  handleChangeFile: (e) ->
    file = e.target.files[0]
    reader = new FileReader
    reader.readAsDataURL file
    # you could also save a reference to this before you invoke the api method:
    that = @
    reader.onload = ->
      that.setState image: reader.result





#    <div class="well">
#    <h4>Leave a Post:</h4>
#          <form role="form" ng-submit="addPost()">
#            <div class="form-group">
#              <input type="text" class="form-control" ng-model="newPost.title" rows="3" required/>
#    <textarea class="form-control" ng-model="newPost.body" rows="3" required></textarea>
#            </div>
#    <label class="btn btn-default btn-file">
#      Choose File <input type="file" ng-model="newPost.image" accept="image/*" style="display: none;" base-sixty-four-input>
#    </label>
#
#            <div ng-controller="CategoryController">
#              <div ng-repeat="category in categories" class="checkbox-inline">
#                <div>
#                  <input type="checkbox" value="{{category.id}}" ng-checked="selection.indexOf(category.id) > -1" ng-click="toggleSelection(category.id)" ng-required="selection == 0"/>
#    <label for="{{category.id}}" ></label>
#                  {{category.name}}
#                </div>
#    </div>
#            </div>
#
#    <button type="submit" class="btn btn-primary">Submit</button>
#          </form>
#    </div>

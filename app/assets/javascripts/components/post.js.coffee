@Post = React.createClass
  render: ->
    React.DOM.div
      className: 'post'
      React.DOM.h2
        className: 'title'
        React.DOM.a
          href: '/posts/' + @props.post.id
          @props.post.title
      React.DOM.p
        className: 'lead'
        'by '
        React.DOM.a
          href: '/users/' + @props.post.user
          'example@mail.com'
      React.DOM.p
        className: 'category'
        'Category: '
        React.DOM.span
          '#ruby'
      React.DOM.p
        className: 'posted-on'
        React.DOM.span
          className: 'glyphicon glyphicon-time'
        ' Posted on ' + @props.post.created_at
      React.DOM.img
        className: "img-responsive"
        src: "/images/post_" + @props.post.id
      React.DOM.p
        className: 'post-body'
        @props.post.body
      React.DOM.a
        className: 'btn btn-primary'
        href: '/posts/' + @props.post.id
        'Read More'
        React.DOM.span
          className: 'glyphicon glyphicon-chevron-right'
      React.DOM.a
        className: 'btn btn-danger'
        'Delete'
      React.DOM.hr
        className: 'post-hr'

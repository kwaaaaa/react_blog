@Posts = React.createClass
  getInitialState: ->
    posts: @props.posts
  getDefaultProps: ->
    posts: []
  addPost: (post) ->
    posts = React.addons.update(@state.posts, { $push: [post] })
    @setState posts: posts
  render: ->
    React.DOM.div
      className: 'posts'
      for post in @state.posts
        React.createElement Post, key: post.id, post: post
      # <!-- Pager -->
      React.DOM.ul
        className: 'pager'
        React.DOM.li
          className: 'previous'
          React.DOM.a
            href: '#'
            'Older'
        React.DOM.li
          className: 'next'
          React.DOM.a
            href: '#'
            'Newer'
      React.createElement PostForm, handleNewPost: @addPost

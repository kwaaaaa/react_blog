class User < ActiveRecord::Base
  has_many :posts, dependent: :destroy
  has_many :comments, dependent: :destroy
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  def user?
    role == 'user'
  end

  def guest?
    role == 'guest'
  end

  def can_update
    scope.can?(:update, object)
  end

end

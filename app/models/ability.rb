class Ability
  include CanCan::Ability

  def initialize(user)
      user ||= User.new # guest user (not logged in)
        if user.user?
        can :destroy, Post do |post|
          post.user == user
        end
        can :destroy, Comment do |comment|
          comment.user == user
        end
        can :create, Post
        can :create, Comment
      elsif user.guest?
        can :read, :all
      end
  end
end

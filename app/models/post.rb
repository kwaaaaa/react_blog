class Post < ActiveRecord::Base
  belongs_to :user
  has_many :comments
  has_and_belongs_to_many :categories
  # validates :title, :body, :category_ids, presence: true
  mount_base64_uploader :image, ImageUploader

  def add_categories(params)
    self.category_ids = params[:category_ids]
  end
end
